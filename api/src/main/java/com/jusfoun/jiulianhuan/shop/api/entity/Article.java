package com.jusfoun.jiulianhuan.shop.api.entity;

import javax.persistence.Id;
import java.util.Date;

/**
 * taocl
 * 文章entity
 */
public class Article implements java.io.Serializable{

    private static final long serialVersionUID = -3560474978426449894L;
    @Id
    private Integer articleId;

    private Integer catId;

    private String title;

    private String author;

    private String authorEmail;

    private String keywords;

    private Integer articleType;

    private Integer isOpen;

    private Date addTime;

    private String fileUrl;

    private Integer openType;

    private String link;

    private String description;

    private String content;

    public Article(){

    }

    public Article(Integer articleId, Integer catId, String title, String author, String authorEmail,
                   String keywords, Integer articleType, Integer isOpen, Date addTime, String fileUrl,
                   Integer openType, String link, String description, String content) {
        this.articleId = articleId;
        this.catId = catId;
        this.title = title;
        this.author = author;
        this.authorEmail = authorEmail;
        this.keywords = keywords;
        this.articleType = articleType;
        this.isOpen = isOpen;
        this.addTime = addTime;
        this.fileUrl = fileUrl;
        this.openType = openType;
        this.link = link;
        this.description = description;
        this.content = content;
    }

    @Override
    public String toString() {
        return "{\"articleId\":\"" + articleId + "\",\"catId\":\"" + catId + "\",\"title\":\"" + title
                + "\",\"author\":\"" + author + "\",\"authorEmail\":\"" + authorEmail + "\",\"keywords\":\"" + keywords
                + "\",\"articleType\":\"" + articleType + "\",\"isOpen\":\"" + isOpen + "\",\"addTime\":\"" + addTime
                + "\",\"fileUrl\":\"" + fileUrl + "\",\"openType\":\"" + openType + "\",\"link\":\"" + link
                + "\",\"description\":\"" + description + "\",\"content\":\"" + content + "\"}  ";
    }


    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author == null ? null : author.trim();
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail == null ? null : authorEmail.trim();
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    public Integer getArticleType() {
        return articleType;
    }

    public void setArticleType(Integer articleType) {
        this.articleType = articleType;
    }

    public Integer getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Integer isOpen) {
        this.isOpen = isOpen;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl == null ? null : fileUrl.trim();
    }

    public Integer getOpenType() {
        return openType;
    }

    public void setOpenType(Integer openType) {
        this.openType = openType;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link == null ? null : link.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}