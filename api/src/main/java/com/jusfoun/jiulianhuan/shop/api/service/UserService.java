package com.jusfoun.jiulianhuan.shop.api.service;

import com.jusfoun.jiulianhuan.shop.api.entity.Pagination;
import com.jusfoun.jiulianhuan.shop.api.entity.User;

/**
 * 会员service
 * @author admin
 *
 */
public interface UserService {
	/**
	 * 根据主键删除会员
	 * @param userId 会员id
	 * @return
	 */
	int deleteByPrimaryKey(String userId);
	/**
	 * 插入会员
	 * @param record
	 * @return
	 */
    int insert(User record);
    /**
     * 插入会员（只插入非空字段）
     * @param record
     * @return
     */
    int insertSelective(User record);
    /**
     * 根据主键查询会员
     * @param userId 会员主键
     * @return
     */
    User findByPrimaryKey(Integer userId);
    /**
     * 有选择更新（非空就更新）
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(User record);
    /**
     * 根据主键更新会员信息
     * @param record
     * @return
     */
    int updateByPrimaryKey(User record);
    /**
     * 分页查询
     * @param user
     * @return
     */
    public Pagination<User> findBypage(User user,Integer pageNum,Integer pageSize);
    
    User findUserByName(String username);
}
