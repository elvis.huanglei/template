package com.jusfoun.jiulianhuan.shop.api.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class GroupGoods extends GroupGoodsKey implements  Serializable{
    private BigDecimal goodsPrice;

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

	@Override
	public String toString() {
		return "GroupGoods [goodsPrice=" + goodsPrice + "]";
	}
    
}