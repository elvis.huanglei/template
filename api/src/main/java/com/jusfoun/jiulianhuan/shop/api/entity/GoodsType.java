package com.jusfoun.jiulianhuan.shop.api.entity;

public class GoodsType implements java.io.Serializable{
    private Integer catId;

    private String catName;

    private Boolean enabled;

    private String attrGroup;

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName == null ? null : catName.trim();
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getAttrGroup() {
        return attrGroup;
    }

    public void setAttrGroup(String attrGroup) {
        this.attrGroup = attrGroup == null ? null : attrGroup.trim();
    }

	@Override
	public String toString() {
		return "GoodsType [catId=" + catId + ", catName=" + catName + ", enabled=" + enabled + ", attrGroup="
				+ attrGroup + "]";
	}
    
}