package com.jusfoun.jiulianhuan.shop.api.entity;


import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author huanglei
 * @created 16/6/9 下午10:41
 */

public class SysUser implements java.io.Serializable{

  private Integer id;
  private String name;
  private String email;
  private String password;
  private Date dob;
  private Set<SysRole> sysRoles = new HashSet<SysRole>(0);

  public SysUser(){}

  public SysUser(String name,String email,String password,Date dob,Set<SysRole> sysRoles){
    this.name = name;
    this.email = email;
    this.password = password;
    this.dob = dob;
    this.sysRoles = sysRoles;
  }

  public SysUser(Integer id ,String name,String email,String password,Date dob,Set<SysRole> sysRoles){
    this.id = id;
    this.name = name;
    this.email = email;
    this.password = password;
    this.dob = dob;
    this.sysRoles = sysRoles;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Date getDob() {
    return dob;
  }

  public void setDob(Date dob) {
    this.dob = dob;
  }

  public Set<SysRole> getSysRoles() {
    return sysRoles;
  }

  public void setSysRoles(Set<SysRole> sysRoles) {
    this.sysRoles = sysRoles;
  }
}
