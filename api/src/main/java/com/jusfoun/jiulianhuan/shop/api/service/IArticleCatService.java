package com.jusfoun.jiulianhuan.shop.api.service;

import com.jusfoun.jiulianhuan.shop.api.entity.ArticleCat;
import com.jusfoun.jiulianhuan.shop.api.entity.Pagination;

import java.util.List;

/**
 * Created by tao on 16/6/15.
 * 文章分类Service
 */
public interface IArticleCatService {
    /**
     * 根据主键删除文章分类
     * @param articleCatId 文章分类id
     * @return
     */
    int deleteByPrimaryKey(Integer articleCatId);
    /**
     * 插入文章分类
     * @param articleCat
     * @return
     */
    int insert(ArticleCat articleCat);
    /**
     * 插入文章分类（只插入非空字段）
     * @param articleCat
     * @return
     */
    int insertSelective(ArticleCat articleCat);
    /**
     * 根据主键查询文章分类
     * @param articleCatId 文章分类主键
     * @return
     */
    ArticleCat findByPrimaryKey(Integer articleCatId);
    /**
     * 有选择更新（非空就更新）
     * @param articleCat
     * @return
     */
    int updateByPrimaryKeySelective(ArticleCat articleCat);
    /**
     * 根据主键更新文章分类信息
     * @param articleCat
     * @return
     */
    int updateByPrimaryKey(ArticleCat articleCat);
    /**
     * 分页查询
     * @param articleCat
     * @return
     */
    public Pagination<ArticleCat> findBypage(ArticleCat articleCat, Integer pageNum, Integer pageSize);


    /**
     * 删除所有的文章分类
     * @param catids
     * @return
     */
    int deleteAllArticleCat(List<Integer> catids);
}
