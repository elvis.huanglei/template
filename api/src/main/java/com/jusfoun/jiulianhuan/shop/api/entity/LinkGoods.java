package com.jusfoun.jiulianhuan.shop.api.entity;

import java.io.Serializable;

public class LinkGoods extends LinkGoodsKey implements Serializable {
    private Boolean isDouble;

    public Boolean getIsDouble() {
        return isDouble;
    }

    public void setIsDouble(Boolean isDouble) {
        this.isDouble = isDouble;
    }

	@Override
	public String toString() {
		return "LinkGoods [isDouble=" + isDouble + "]";
	}
    
}