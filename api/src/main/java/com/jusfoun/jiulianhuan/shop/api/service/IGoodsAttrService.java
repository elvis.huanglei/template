/**
 * All rights Reserved, Designed By ZTE-ITS
 * Copyright:    Copyright(C) 2016-2020
 * Company       JUSFOUN GuoZhiFeng LTD.
 * @author:    郭志峰
 * @version    V1.0 
 * Createdate:         2016年6月15日 下午5:47:24
 * Modification  History:
 * Date         Author        Version        Discription
 * -----------------------------------------------------------------------------------
 * 2016年6月15日       guozhifeng         1.0             1.0
 * Why & What is modified: <修改原因描述>
 */
package com.jusfoun.jiulianhuan.shop.api.service;

import com.jusfoun.jiulianhuan.shop.api.entity.GoodsAttr;

/**
 * 商品的扩展属性表的操作
 * 
 * @ClassName: IGoodsAttrService
 * @Description: 商品的扩展属性表的操作
 * @author Timothy guozhifengvip@163.com
 * @date 2016年6月15日 下午5:47:39
 *
 */
public interface IGoodsAttrService {
    /**
     * 根据主键删除
     * @Title: deleteByPrimaryKey 
     * @Description:根据主键删除
     * @param @param goodsAttrId
     * @param @return    设定文件 
     * @return int    返回类型 
     * @throws
     */
	int deleteByPrimaryKey(Integer goodsAttrId);
    /**
     * 插入一条数据
     * @Title: insert 
     * @Description: 插入一条数据
     * @param @param record
     * @param @return    设定文件 
     * @return int    返回类型 
     * @throws
     */
	int insert(GoodsAttr record);
    /**
     * 插入一条数据,只插入不为null的字段,不会影响有默认值的字段
     * @Title: insertSelective 
     * @Description: 插入一条数据,只插入不为null的字段,不会影响有默认值的字段 
     * @param @param record
     * @param @return    设定文件 
     * @return int    返回类型 
     * @throws
     */
	int insertSelective(GoodsAttr record);
     /**
      * 根据主键查找一条数据
      * @Title: findByPrimaryKey 
      * @Description: 根据主键查找一条数据
      * @param @param goodsAttrId
      * @param @return    设定文件 
      * @return GoodsAttr    返回类型 
      * @throws
      */
	GoodsAttr findByPrimaryKey(Integer goodsAttrId);
      /**
       * 更新不为空的数据
       * @Title: updateByPrimaryKeySelective 
       * @Description: 更新不为空的数据
       * @param @param record
       * @param @return    设定文件 
       * @return int    返回类型 
       * @throws
       */
	int updateByPrimaryKeySelective(GoodsAttr record);
       /**
        * 
        * @Title: updateByPrimaryKeyWithBLOBs 
        * @Description: 
        * @param @param record
        * @param @return    设定文件 
        * @return int    返回类型 
        * @throws
        */
	int updateByPrimaryKeyWithBLOBs(GoodsAttr record);
        /**
         * 根据主键更新
         * @Title: updateByPrimaryKey 
         * @Description: 根据主键更新
         * @param @param record
         * @param @return    设定文件 
         * @return int    返回类型 
         * @throws
         */
	int updateByPrimaryKey(GoodsAttr record);
}
