package com.jusfoun.jiulianhuan.shop.api.entity;

public class GoodsAttr implements java.io.Serializable{
    private Integer goodsAttrId;

    private Integer goodsId;

    private Integer attrId;

    private String attrPrice;

    private String attrValue;

    public Integer getGoodsAttrId() {
        return goodsAttrId;
    }

    public void setGoodsAttrId(Integer goodsAttrId) {
        this.goodsAttrId = goodsAttrId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getAttrId() {
        return attrId;
    }

    public void setAttrId(Integer attrId) {
        this.attrId = attrId;
    }

    public String getAttrPrice() {
        return attrPrice;
    }

    public void setAttrPrice(String attrPrice) {
        this.attrPrice = attrPrice == null ? null : attrPrice.trim();
    }

    public String getAttrValue() {
        return attrValue;
    }

    public void setAttrValue(String attrValue) {
        this.attrValue = attrValue == null ? null : attrValue.trim();
    }

	@Override
	public String toString() {
		return "GoodsAttr [goodsAttrId=" + goodsAttrId + ", goodsId=" + goodsId + ", attrId=" + attrId + ", attrPrice="
				+ attrPrice + ", attrValue=" + attrValue + "]";
	}
    
}