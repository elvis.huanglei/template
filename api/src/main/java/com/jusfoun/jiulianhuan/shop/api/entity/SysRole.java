package com.jusfoun.jiulianhuan.shop.api.entity;

/**
 * @author huanglei
 * @created 16/6/9 下午10:44
 */
public class SysRole implements java.io.Serializable{

  private Integer id;
  private SysUser sysUser;
  private String name;

  public SysRole(){}

  public SysRole(SysUser sysUser){
    this.sysUser = sysUser;
  }

  public SysRole(SysUser sysUser, String name){
    this.sysUser = sysUser;
    this.name = name;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public SysUser getSysUser() {
    return sysUser;
  }

  public void setSysUser(SysUser sysUser) {
    this.sysUser = sysUser;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
