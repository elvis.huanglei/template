package com.jusfoun.jiulianhuan.shop.api.entity;

public class GoodsTypeAttribute implements java.io.Serializable{
    private Integer attrId;

    private Integer catId;

    private String attrName;

    private Boolean attrInputType;

    private Boolean attrType;

    private Boolean attrIndex;

    private Byte sortOrder;

    private Boolean isLinked;

    private Boolean attrGroup;

    private String attrValues;

    public Integer getAttrId() {
        return attrId;
    }

    public void setAttrId(Integer attrId) {
        this.attrId = attrId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName == null ? null : attrName.trim();
    }

    public Boolean getAttrInputType() {
        return attrInputType;
    }

    public void setAttrInputType(Boolean attrInputType) {
        this.attrInputType = attrInputType;
    }

    public Boolean getAttrType() {
        return attrType;
    }

    public void setAttrType(Boolean attrType) {
        this.attrType = attrType;
    }

    public Boolean getAttrIndex() {
        return attrIndex;
    }

    public void setAttrIndex(Boolean attrIndex) {
        this.attrIndex = attrIndex;
    }

    public Byte getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Byte sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Boolean getIsLinked() {
        return isLinked;
    }

    public void setIsLinked(Boolean isLinked) {
        this.isLinked = isLinked;
    }

    public Boolean getAttrGroup() {
        return attrGroup;
    }

    public void setAttrGroup(Boolean attrGroup) {
        this.attrGroup = attrGroup;
    }

    public String getAttrValues() {
        return attrValues;
    }

    public void setAttrValues(String attrValues) {
        this.attrValues = attrValues == null ? null : attrValues.trim();
    }

	@Override
	public String toString() {
		return "GoodsTypeAttribute [attrId=" + attrId + ", catId=" + catId + ", attrName=" + attrName
				+ ", attrInputType=" + attrInputType + ", attrType=" + attrType + ", attrIndex=" + attrIndex
				+ ", sortOrder=" + sortOrder + ", isLinked=" + isLinked + ", attrGroup=" + attrGroup + ", attrValues="
				+ attrValues + "]";
	}
    
}