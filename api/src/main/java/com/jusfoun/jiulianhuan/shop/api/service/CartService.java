package com.jusfoun.jiulianhuan.shop.api.service;

import com.jusfoun.jiulianhuan.shop.api.entity.Cart;

public interface CartService {
    int deleteByPrimaryKey(Integer recId);

    int insert(Cart record);

    int insertSelective(Cart record);

    Cart selectByPrimaryKey(Integer recId);

    int updateByPrimaryKeySelective(Cart record);

    int updateByPrimaryKeyWithBLOBs(Cart record);

    int updateByPrimaryKey(Cart record);
}