package com.jusfoun.jiulianhuan.shop.api.entity;

import javax.persistence.Id;

/**
 * taocl
 * 文章分类entity
 */
public class ArticleCat implements java.io.Serializable{


    private static final long serialVersionUID = 8424269074301343454L;
    @Id
    private Integer catId;
    private String catName;
    private String catDesc;
    private Integer catFlag;



    public ArticleCat() {
    }

    public ArticleCat(Integer catId, String catName, String catDesc) {
        this.catId = catId;
        this.catName = catName;
        this.catDesc = catDesc;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatDesc() {
        return catDesc;
    }

    public void setCatDesc(String catDesc) {
        this.catDesc = catDesc;
    }

    public Integer getCatFlag() {
        return catFlag;
    }

    public void setCatFlag(Integer catFlag) {
        this.catFlag = catFlag;
    }

    @Override
    public String toString() {
        return "{\"catId\":\"" + catId + "\",\"catName\":\"" + catName + "\",\"catDesc\":\"" + catDesc
                + "\",\"catFlag\":\"" + catFlag + "\"}  ";
    }
}