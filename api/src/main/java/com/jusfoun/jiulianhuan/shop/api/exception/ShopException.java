package com.jusfoun.jiulianhuan.shop.api.exception;
/**
 * 自定义异常
 * @author admin
 *
 */
public class ShopException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8421450006891206755L;

	public ShopException(){
		super();
	}
	
	public ShopException(String msg){
		super(msg);
	}
}
