package com.jusfoun.jiulianhuan.shop.api.entity;

import java.io.Serializable;

public class LinkGoodsKey implements Serializable {
    private Integer goodsId;

    private Integer linkGoodsId;

    private Integer adminId;

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getLinkGoodsId() {
        return linkGoodsId;
    }

    public void setLinkGoodsId(Integer linkGoodsId) {
        this.linkGoodsId = linkGoodsId;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

	@Override
	public String toString() {
		return "LinkGoodsKey [goodsId=" + goodsId + ", linkGoodsId=" + linkGoodsId + ", adminId=" + adminId + "]";
	}
    
}