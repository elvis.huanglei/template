package com.jusfoun.jiulianhuan.shop.api.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Goods  implements Serializable{
    private Integer goodsId;

    private Integer catId;

    private String goodsSn;

    private String goodsName;

    private String goodsNameStyle;

    private Integer clickCount;

    private Integer brandId;

    private String providerName;

    private Integer goodsNumber;

    private BigDecimal goodsWeight;

    private BigDecimal marketPrice;

    private Short virtualSales;

    private BigDecimal shopPrice;

    private BigDecimal promotePrice;

    private Date promoteStartDate;

    private Date promoteEndDate;

    private Byte warnNumber;

    private String keywords;

    private String goodsBrief;

    private String goodsThumb;

    private String goodsImg;

    private String originalImg;

    private Byte isReal;

    private String extensionCode;

    private Boolean isOnSale;

    private Boolean isAloneSale;

    private Boolean isShipping;

    private Integer integral;

    private Date addTime;

    private Short sortOrder;

    private Boolean isDelete;

    private Boolean isBest;

    private Boolean isNew;

    private Boolean isHot;

    private Boolean isPromote;

    private Byte bonusTypeId;

    private Date lastUpdate;

    private Integer goodsType;

    private String sellerNote;

    private Integer giveIntegral;

    private Integer rankIntegral;

    private Integer suppliersId;

    private Boolean isCheck;

    private String goodsDesc;

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getGoodsSn() {
        return goodsSn;
    }

    public void setGoodsSn(String goodsSn) {
        this.goodsSn = goodsSn == null ? null : goodsSn.trim();
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName == null ? null : goodsName.trim();
    }

    public String getGoodsNameStyle() {
        return goodsNameStyle;
    }

    public void setGoodsNameStyle(String goodsNameStyle) {
        this.goodsNameStyle = goodsNameStyle == null ? null : goodsNameStyle.trim();
    }

    public Integer getClickCount() {
        return clickCount;
    }

    public void setClickCount(Integer clickCount) {
        this.clickCount = clickCount;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName == null ? null : providerName.trim();
    }

    public Integer getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(Integer goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public BigDecimal getGoodsWeight() {
        return goodsWeight;
    }

    public void setGoodsWeight(BigDecimal goodsWeight) {
        this.goodsWeight = goodsWeight;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Short getVirtualSales() {
        return virtualSales;
    }

    public void setVirtualSales(Short virtualSales) {
        this.virtualSales = virtualSales;
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }

    public BigDecimal getPromotePrice() {
        return promotePrice;
    }

    public void setPromotePrice(BigDecimal promotePrice) {
        this.promotePrice = promotePrice;
    }

    public Date getPromoteStartDate() {
        return promoteStartDate;
    }

    public void setPromoteStartDate(Date promoteStartDate) {
        this.promoteStartDate = promoteStartDate;
    }

    public Date getPromoteEndDate() {
        return promoteEndDate;
    }

    public void setPromoteEndDate(Date promoteEndDate) {
        this.promoteEndDate = promoteEndDate;
    }

    public Byte getWarnNumber() {
        return warnNumber;
    }

    public void setWarnNumber(Byte warnNumber) {
        this.warnNumber = warnNumber;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    public String getGoodsBrief() {
        return goodsBrief;
    }

    public void setGoodsBrief(String goodsBrief) {
        this.goodsBrief = goodsBrief == null ? null : goodsBrief.trim();
    }

    public String getGoodsThumb() {
        return goodsThumb;
    }

    public void setGoodsThumb(String goodsThumb) {
        this.goodsThumb = goodsThumb == null ? null : goodsThumb.trim();
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg == null ? null : goodsImg.trim();
    }

    public String getOriginalImg() {
        return originalImg;
    }

    public void setOriginalImg(String originalImg) {
        this.originalImg = originalImg == null ? null : originalImg.trim();
    }

    public Byte getIsReal() {
        return isReal;
    }

    public void setIsReal(Byte isReal) {
        this.isReal = isReal;
    }

    public String getExtensionCode() {
        return extensionCode;
    }

    public void setExtensionCode(String extensionCode) {
        this.extensionCode = extensionCode == null ? null : extensionCode.trim();
    }

    public Boolean getIsOnSale() {
        return isOnSale;
    }

    public void setIsOnSale(Boolean isOnSale) {
        this.isOnSale = isOnSale;
    }

    public Boolean getIsAloneSale() {
        return isAloneSale;
    }

    public void setIsAloneSale(Boolean isAloneSale) {
        this.isAloneSale = isAloneSale;
    }

    public Boolean getIsShipping() {
        return isShipping;
    }

    public void setIsShipping(Boolean isShipping) {
        this.isShipping = isShipping;
    }

    public Integer getIntegral() {
        return integral;
    }

    public void setIntegral(Integer integral) {
        this.integral = integral;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Short getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Short sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsBest() {
        return isBest;
    }

    public void setIsBest(Boolean isBest) {
        this.isBest = isBest;
    }

    public Boolean getIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public Boolean getIsHot() {
        return isHot;
    }

    public void setIsHot(Boolean isHot) {
        this.isHot = isHot;
    }

    public Boolean getIsPromote() {
        return isPromote;
    }

    public void setIsPromote(Boolean isPromote) {
        this.isPromote = isPromote;
    }

    public Byte getBonusTypeId() {
        return bonusTypeId;
    }

    public void setBonusTypeId(Byte bonusTypeId) {
        this.bonusTypeId = bonusTypeId;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(Integer goodsType) {
        this.goodsType = goodsType;
    }

    public String getSellerNote() {
        return sellerNote;
    }

    public void setSellerNote(String sellerNote) {
        this.sellerNote = sellerNote == null ? null : sellerNote.trim();
    }

    public Integer getGiveIntegral() {
        return giveIntegral;
    }

    public void setGiveIntegral(Integer giveIntegral) {
        this.giveIntegral = giveIntegral;
    }

    public Integer getRankIntegral() {
        return rankIntegral;
    }

    public void setRankIntegral(Integer rankIntegral) {
        this.rankIntegral = rankIntegral;
    }

    public Integer getSuppliersId() {
        return suppliersId;
    }

    public void setSuppliersId(Integer suppliersId) {
        this.suppliersId = suppliersId;
    }

    public Boolean getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(Boolean isCheck) {
        this.isCheck = isCheck;
    }

    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc == null ? null : goodsDesc.trim();
    }

	@Override
	public String toString() {
		return "Goods [goodsId=" + goodsId + ", catId=" + catId + ", goodsSn=" + goodsSn + ", goodsName=" + goodsName
				+ ", goodsNameStyle=" + goodsNameStyle + ", clickCount=" + clickCount + ", brandId=" + brandId
				+ ", providerName=" + providerName + ", goodsNumber=" + goodsNumber + ", goodsWeight=" + goodsWeight
				+ ", marketPrice=" + marketPrice + ", virtualSales=" + virtualSales + ", shopPrice=" + shopPrice
				+ ", promotePrice=" + promotePrice + ", promoteStartDate=" + promoteStartDate + ", promoteEndDate="
				+ promoteEndDate + ", warnNumber=" + warnNumber + ", keywords=" + keywords + ", goodsBrief="
				+ goodsBrief + ", goodsThumb=" + goodsThumb + ", goodsImg=" + goodsImg + ", originalImg=" + originalImg
				+ ", isReal=" + isReal + ", extensionCode=" + extensionCode + ", isOnSale=" + isOnSale
				+ ", isAloneSale=" + isAloneSale + ", isShipping=" + isShipping + ", integral=" + integral
				+ ", addTime=" + addTime + ", sortOrder=" + sortOrder + ", isDelete=" + isDelete + ", isBest=" + isBest
				+ ", isNew=" + isNew + ", isHot=" + isHot + ", isPromote=" + isPromote + ", bonusTypeId=" + bonusTypeId
				+ ", lastUpdate=" + lastUpdate + ", goodsType=" + goodsType + ", sellerNote=" + sellerNote
				+ ", giveIntegral=" + giveIntegral + ", rankIntegral=" + rankIntegral + ", suppliersId=" + suppliersId
				+ ", isCheck=" + isCheck + ", goodsDesc=" + goodsDesc + "]";
	}
    
}