package com.jusfoun.jiulianhuan.shop.api.entity;

import java.math.BigDecimal;
import java.util.Date;

public class FavourableActivity {
    private Integer actId;

    private String actName;

    private Date startTime;

    private Date endTime;

    private String userRank;

    private Byte actRange;

    private String actRangeExt;

    private BigDecimal minAmount;

    private BigDecimal maxAmount;

    private Byte actType;

    private BigDecimal actTypeExt;

    private Byte sortOrder;

    private String gift;

    public Integer getActId() {
        return actId;
    }

    public void setActId(Integer actId) {
        this.actId = actId;
    }

    public String getActName() {
        return actName;
    }

    public void setActName(String actName) {
        this.actName = actName == null ? null : actName.trim();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getUserRank() {
        return userRank;
    }

    public void setUserRank(String userRank) {
        this.userRank = userRank == null ? null : userRank.trim();
    }

    public Byte getActRange() {
        return actRange;
    }

    public void setActRange(Byte actRange) {
        this.actRange = actRange;
    }

    public String getActRangeExt() {
        return actRangeExt;
    }

    public void setActRangeExt(String actRangeExt) {
        this.actRangeExt = actRangeExt == null ? null : actRangeExt.trim();
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Byte getActType() {
        return actType;
    }

    public void setActType(Byte actType) {
        this.actType = actType;
    }

    public BigDecimal getActTypeExt() {
        return actTypeExt;
    }

    public void setActTypeExt(BigDecimal actTypeExt) {
        this.actTypeExt = actTypeExt;
    }

    public Byte getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Byte sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getGift() {
        return gift;
    }

    public void setGift(String gift) {
        this.gift = gift == null ? null : gift.trim();
    }
}