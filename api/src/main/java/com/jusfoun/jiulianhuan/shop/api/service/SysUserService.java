package com.jusfoun.jiulianhuan.shop.api.service;

import com.jusfoun.jiulianhuan.shop.api.entity.SysUser;

/**
 * @author huanglei
 * @created 16/6/10 上午9:28
 */
public interface SysUserService {

  SysUser findSysUserById(int id);

  SysUser findSysUserByEmail(String email);

  void update(SysUser user);
}
