package com.jusfoun.jiulianhuan.shop.api.entity;

import java.io.Serializable;

public class GroupGoodsKey implements Serializable{
    private Integer parentId;

    private Integer goodsId;

    private Integer adminId;

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

	@Override
	public String toString() {
		return "GroupGoodsKey [parentId=" + parentId + ", goodsId=" + goodsId + ", adminId=" + adminId + "]";
	}
    
}