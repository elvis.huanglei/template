package com.jusfoun.jiulianhuan.shop.api.entity;

public class UserRank {
    private Integer rankId;

    private String rankName;

    private Integer minPoints;

    private Integer maxPoints;

    private Byte discount;

    private Boolean showPrice;

    private Boolean specialRank;

    public Integer getRankId() {
        return rankId;
    }

    public void setRankId(Integer rankId) {
        this.rankId = rankId;
    }

    public String getRankName() {
        return rankName;
    }

    public void setRankName(String rankName) {
        this.rankName = rankName == null ? null : rankName.trim();
    }

    public Integer getMinPoints() {
        return minPoints;
    }

    public void setMinPoints(Integer minPoints) {
        this.minPoints = minPoints;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }

    public Byte getDiscount() {
        return discount;
    }

    public void setDiscount(Byte discount) {
        this.discount = discount;
    }

    public Boolean getShowPrice() {
        return showPrice;
    }

    public void setShowPrice(Boolean showPrice) {
        this.showPrice = showPrice;
    }

    public Boolean getSpecialRank() {
        return specialRank;
    }

    public void setSpecialRank(Boolean specialRank) {
        this.specialRank = specialRank;
    }
}