/**
 * All rights Reserved, Designed By ZTE-ITS
 * Copyright:    Copyright(C) 2016-2020
 * Company       JUSFOUN GuoZhiFeng LTD.
 * @author:    郭志峰
 * @version    V1.0 
 * Createdate:         2016年6月15日 下午4:16:12
 * Modification  History:
 * Date         Author        Version        Discription
 * -----------------------------------------------------------------------------------
 * 2016年6月15日       guozhifeng         1.0             1.0
 * Why & What is modified: <修改原因描述>
 */
package com.jusfoun.jiulianhuan.shop.api.service;

import com.jusfoun.jiulianhuan.shop.api.entity.Goods;

/**
 * 商品表的操作接口
 * @ClassName: IGoods
 * @Description:  商品表的操作接口
 * @author Timothy guozhifengvip@163.com
 * @date 2016年6月15日 下午4:16:51
 *
 */
public interface IGoodsService {
	/**
	 * 根据主键删除一条商品数据
	 * @Title: deleteByPrimaryKey 
	 * @Description: 
	 * @param @param goodsId
	 * @param @return    设定文件 
	 * @return int    返回类型 
	 * @throws
	 */
	int deleteByPrimaryKey(Integer goodsId);
    /**
     * 插入一条商品信息
     * @Title: insert 
     * @Description:插入一条商品信息
     * @param @param record
     * @param @return    设定文件 
     * @return int    返回类型 
     * @throws
     */
	int insert(Goods record);
    /**
     * 插入一条数据,只插入不为null的字段,不会影响有默认值的字段
     * @Title: insertSelective 
     * @Description: 插入一条数据,只插入不为null的字段,不会影响有默认值的字段
     * @param @param record
     * @param @return    设定文件 
     * @return int    返回类型 
     * @throws
     */
	int insertSelective(Goods record);
     /**
      * 根据主键查找一条商品信息
      * @Title: findByPrimaryKey 
      * @Description:根据主键查找一条商品信息
      * @param @param goodsId
      * @param @return    设定文件 
      * @return Goods    返回类型 
      * @throws
      */
	Goods findByPrimaryKey(Integer goodsId);
    /**
     * 根据主键更新一条商品信息（不为空）
     * @Title: updateByPrimaryKeySelective 
     * @Description: 根据主键更新一条商品信息（不为空）
     * @param @param record
     * @param @return    设定文件 
     * @return int    返回类型 
     * @throws
     */
	int updateByPrimaryKeySelective(Goods record);
     /**
      * 
      * @Title: updateByPrimaryKeyWithBLOBs 
      * @Description: 
      * @param @param record
      * @param @return    设定文件 
      * @return int    返回类型 
      * @throws
      */
	int updateByPrimaryKeyWithBLOBs(Goods record);
    /**
     * 根据主键更新一条信息
     * @Title: updateByPrimaryKey 
     * @Description: 根据主键更新一条信息
     * @param @param record
     * @param @return    设定文件 
     * @return int    返回类型 
     * @throws
     */
	int updateByPrimaryKey(Goods record);

}
