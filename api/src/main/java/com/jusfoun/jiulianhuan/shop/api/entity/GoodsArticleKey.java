package com.jusfoun.jiulianhuan.shop.api.entity;

public class GoodsArticleKey implements java.io.Serializable{
    private Integer goodsId;

    private Integer articleId;

    private Integer adminId;

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

	@Override
	public String toString() {
		return "GoodsArticleKey [goodsId=" + goodsId + ", articleId=" + articleId + ", adminId=" + adminId + "]";
	}
    
}