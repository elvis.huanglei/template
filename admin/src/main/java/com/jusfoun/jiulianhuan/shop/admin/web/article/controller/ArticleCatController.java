package com.jusfoun.jiulianhuan.shop.admin.web.article.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jusfoun.jiulianhuan.shop.admin.web.common.Result;
import com.jusfoun.jiulianhuan.shop.api.entity.ArticleCat;
import com.jusfoun.jiulianhuan.shop.api.entity.Pagination;
import com.jusfoun.jiulianhuan.shop.api.entity.User;
import com.jusfoun.jiulianhuan.shop.api.exception.ShopException;
import com.jusfoun.jiulianhuan.shop.api.service.IArticleCatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tao on 16/6/16.
 */
@RestController
@RequestMapping("/articlecat")
public class ArticleCatController {
    private static final Logger logger = LoggerFactory.getLogger(ArticleCatController.class);
    @Reference
    private IArticleCatService service;
    /**
     * 插入文章列表
     * @param articleCat
     * @return
     */
    @RequestMapping("/insert")
    public Result insert(ArticleCat articleCat){
        Result result = new Result();
        result.setRetCode(Result.RETCODE_SUCCESS);
        try{
            service.insert(articleCat);
        }catch(ShopException e){
            result.setRetCode(Result.RETCODE_ERROR);
            result.setErrMsg(e.getMessage());
        }catch(Exception e){
            logger.error("插入错误",e);
            result.setRetCode(Result.RETCODE_ERROR);
            result.setErrMsg("系统异常");
        }

        return result;
    }

    /**
     * 删除文章列表
     * @param catId
     * @return
     */
    @RequestMapping("/delete")
    public Result delete(Integer catId){
        Result result = new Result();
        result.setRetCode(Result.RETCODE_SUCCESS);
        try{
            service.deleteByPrimaryKey(catId);
        }catch(ShopException e){
            result.setRetCode(Result.RETCODE_ERROR);
            result.setErrMsg(e.getMessage());
        }catch(Exception e){
            logger.error("删除错误",e);
            result.setRetCode(Result.RETCODE_ERROR);
            result.setErrMsg("系统异常");
        }

        return result;
    }
    /**
     * 修改文章类型
     * @param articleCat
     * @return
     */
    @RequestMapping("/update")
    public Result update(ArticleCat articleCat){
        Result result = new Result();
        result.setRetCode(Result.RETCODE_SUCCESS);
        try{
            service.updateByPrimaryKeySelective(articleCat);
        }catch(ShopException e){
            result.setRetCode(Result.RETCODE_ERROR);
            result.setErrMsg(e.getMessage());
        }catch(Exception e){
            logger.error("修改错误",e);
            result.setRetCode(Result.RETCODE_ERROR);
            result.setErrMsg("系统异常");
        }

        return result;
    }
    /**
     * 分页查询文章类别
     * @param articleCat
     * @return
     */
    @RequestMapping("/list")
    public Result findByPage(ArticleCat articleCat, @RequestParam(defaultValue="1")Integer pageNum, @RequestParam(defaultValue="15")Integer pageSize){
        Result result = new Result();
        result.setRetCode(Result.RETCODE_SUCCESS);
        try{
            Pagination<ArticleCat> p = service.findBypage(articleCat,pageNum,pageSize);
            result.setData(p);
        }catch(ShopException e){
            result.setRetCode(Result.RETCODE_ERROR);
            result.setErrMsg(e.getMessage());
        }catch(Exception e){
            logger.error("分页查询错误",e);
            result.setRetCode(Result.RETCODE_ERROR);
            result.setErrMsg("系统异常");
        }

        return result;
    }
    /**
     * 会员详情
     * @param articlecatid
     * @return
     */
    @RequestMapping("/detail")
    public Result detail(Integer articlecatid){
        Result result = new Result();
        result.setRetCode(Result.RETCODE_SUCCESS);
        try{
            ArticleCat articleCat = service.findByPrimaryKey(articlecatid);
            result.setData(articleCat);
        }catch(ShopException e){
            result.setRetCode(Result.RETCODE_ERROR);
            result.setErrMsg(e.getMessage());
        }catch(Exception e){
            logger.error("会员详情错误",e);
            result.setRetCode(Result.RETCODE_ERROR);
            result.setErrMsg("系统异常");
        }

        return result;
    }



}