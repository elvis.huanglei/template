package com.jusfoun.jiulianhuan.shop.admin.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * @author huanglei
 * @created 16/6/11 上午3:33
 */
@Configuration
@ImportResource("classpath:dubbox/consumer.xml")
public class DubboxConfiguration {

}
