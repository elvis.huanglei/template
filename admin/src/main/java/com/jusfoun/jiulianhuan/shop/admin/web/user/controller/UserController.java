package com.jusfoun.jiulianhuan.shop.admin.web.user.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jusfoun.jiulianhuan.shop.admin.web.auth.controller.AuthController;
import com.jusfoun.jiulianhuan.shop.admin.web.common.Result;
import com.jusfoun.jiulianhuan.shop.api.entity.Pagination;
import com.jusfoun.jiulianhuan.shop.api.entity.User;
import com.jusfoun.jiulianhuan.shop.api.exception.ShopException;
import com.jusfoun.jiulianhuan.shop.api.service.UserService;
/**
 * 
 * @author zhanghanqing
 * @created 2016年6月16日
 */
@RestController
@RequestMapping("/user")
public class UserController {
	private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
	@Reference
	private UserService service;
	/**
	 * 插入会员
	 * @param user
	 * @return
	 */
	@RequestMapping("/insert")
	public Result insert(User user){
		Result result = new Result();
		result.setRetCode(Result.RETCODE_SUCCESS);
		try{
			service.insert(user);
		}catch(ShopException e){
			result.setRetCode(Result.RETCODE_ERROR);
			result.setErrMsg(e.getMessage());
		}catch(Exception e){
			logger.error("插入错误",e);
			result.setRetCode(Result.RETCODE_ERROR);
			result.setErrMsg("系统异常");
		}
		
		return result;
	}
	
	/**
	 * 删除会员
	 * @param userId
	 * @return
	 */
	@RequestMapping("/delete")
	public Result delete(String userId){
		Result result = new Result();
		result.setRetCode(Result.RETCODE_SUCCESS);
		try{
			service.deleteByPrimaryKey(userId);
		}catch(ShopException e){
			result.setRetCode(Result.RETCODE_ERROR);
			result.setErrMsg(e.getMessage());
		}catch(Exception e){
			logger.error("删除错误",e);
			result.setRetCode(Result.RETCODE_ERROR);
			result.setErrMsg("系统异常");
		}
		
		return result;
	}
	/**
	 * 修改会员
	 * @param user
	 * @return
	 */
	@RequestMapping("/update")
	public Result update(User user){
		Result result = new Result();
		result.setRetCode(Result.RETCODE_SUCCESS);
		try{
			service.updateByPrimaryKeySelective(user);
		}catch(ShopException e){
			result.setRetCode(Result.RETCODE_ERROR);
			result.setErrMsg(e.getMessage());
		}catch(Exception e){
			logger.error("修改错误",e);
			result.setRetCode(Result.RETCODE_ERROR);
			result.setErrMsg("系统异常");
		}
		
		return result;
	}
	/**
	 * 分页查询
	 * @param user
	 * @return
	 */
	@RequestMapping("/list")
	public Result findByPage(User user,@RequestParam(defaultValue="1")Integer pageNum,@RequestParam(defaultValue="15")Integer pageSize){
		Result result = new Result();
		result.setRetCode(Result.RETCODE_SUCCESS);
		try{
			Pagination<User> p = service.findBypage(user,pageNum,pageSize);
			result.setData(p);
		}catch(ShopException e){
			result.setRetCode(Result.RETCODE_ERROR);
			result.setErrMsg(e.getMessage());
		}catch(Exception e){
			logger.error("分页查询错误",e);
			result.setRetCode(Result.RETCODE_ERROR);
			result.setErrMsg("系统异常");
		}
		
		return result;
	}
	/**
	 * 会员详情
	 * @param userId
	 * @return
	 */
	@RequestMapping("/detail")
	public Result detail(Integer userId){
		Result result = new Result();
		result.setRetCode(Result.RETCODE_SUCCESS);
		try{
			User user = service.findByPrimaryKey(userId);
			result.setData(user);
		}catch(ShopException e){
			result.setRetCode(Result.RETCODE_ERROR);
			result.setErrMsg(e.getMessage());
		}catch(Exception e){
			logger.error("会员详情错误",e);
			result.setRetCode(Result.RETCODE_ERROR);
			result.setErrMsg("系统异常");
		}
		
		return result;
	}
	
}
