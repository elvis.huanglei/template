package com.jusfoun.jiulianhuan.shop.admin.web.home;


import com.jusfoun.jiulianhuan.shop.api.entity.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * IndexController
 *
 * @author elvis
 * @version 1.0
 * @date 16/6/6 下午2:56
 **/
@Controller
public class IndexController {

    private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

    @RequestMapping(value = "/",method = RequestMethod.GET)
    String home(Model model) {

        SecurityContext ctx = SecurityContextHolder.getContext();
        Authentication auth = ctx.getAuthentication();
        if(auth.getPrincipal() instanceof UserDetails){
            SysUser user = (SysUser)auth.getPrincipal();
            model.addAttribute("user",user);
        }
        System.out.println("index..........");
        return "index";
    }

}