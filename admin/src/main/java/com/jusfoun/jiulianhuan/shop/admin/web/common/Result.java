package com.jusfoun.jiulianhuan.shop.admin.web.common;
/**
 * 页面返回对象
 * @author admin
 *
 */
public class Result {
	public static Integer RETCODE_SUCCESS=1;
	public static Integer RETCODE_ERROR=0;
	/**
	 * 错误信息
	 */
	private String errMsg;
	/**
	 * 状态码
	 */
	private Integer retCode;
	/**
	 * 实体数据
	 */
	private Object data;
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public Integer getRetCode() {
		return retCode;
	}
	public void setRetCode(Integer retCode) {
		this.retCode = retCode;
	}
	
}
