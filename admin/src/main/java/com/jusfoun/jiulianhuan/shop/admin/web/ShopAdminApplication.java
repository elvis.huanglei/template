package com.jusfoun.jiulianhuan.shop.admin.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;



@Configuration
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
//@EnableAutoConfiguration
@ComponentScan
//@SpringBootApplication // same as @Configuration @EnableAutoConfiguration @ComponentScan
public class ShopAdminApplication{

	private static final Logger logger = LoggerFactory.getLogger(ShopAdminApplication.class);

//	@PostConstruct
//	public void logSomething() {
//		logger.debug("Sample Debug Message");
//		logger.trace("Sample Trace Message");
//	}

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(ShopAdminApplication.class, args);

//    SysUserService sysUserService = (SysUserService)ctx.getBean("SysUserServiceImpl");
//    SysUser su = sysUserService.findSysUserById(1);
//    BCryptPasswordEncoder bc = new BCryptPasswordEncoder(4);
//    su.setPassword(bc.encode("111111"));
//    sysUserService.update(su);
	}


}
