package com.jusfoun.jiulianhuan.shop.admin.web.config;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.jusfoun.jiulianhuan.shop.api.entity.SecurityUser;
import com.jusfoun.jiulianhuan.shop.api.entity.SysUser;
import com.jusfoun.jiulianhuan.shop.api.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * @author huanglei
 * @created 16/6/10 上午12:10
 */
@Component
public class CustomUserDetailsService implements UserDetailsService{

  @Reference
  private SysUserService sysUserService;

  public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
    SysUser user = sysUserService.findSysUserByEmail(s);
    if(user == null){
      throw  new UsernameNotFoundException("UserName "+ s +"not found");
    }
    return new SecurityUser(user);
  }
}
