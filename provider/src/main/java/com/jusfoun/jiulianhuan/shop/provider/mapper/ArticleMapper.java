package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.Article;
import com.jusfoun.jiulianhuan.shop.provider.ext.MyMapper;

/**
 * taocl
 * 文章mapper
 */
public interface ArticleMapper extends MyMapper<Article>{
}