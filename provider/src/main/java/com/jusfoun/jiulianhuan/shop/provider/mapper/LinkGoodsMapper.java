package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.LinkGoods;
import com.jusfoun.jiulianhuan.shop.api.entity.LinkGoodsKey;

public interface LinkGoodsMapper {
    int deleteByPrimaryKey(LinkGoodsKey key);

    int insert(LinkGoods record);

    int insertSelective(LinkGoods record);

    LinkGoods selectByPrimaryKey(LinkGoodsKey key);

    int updateByPrimaryKeySelective(LinkGoods record);

    int updateByPrimaryKey(LinkGoods record);
}