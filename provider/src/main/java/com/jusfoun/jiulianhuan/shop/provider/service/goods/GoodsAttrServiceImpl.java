/**
 * All rights Reserved, Designed By ZTE-ITS
 * Copyright:    Copyright(C) 2016-2020
 * Company       JUSFOUN GuoZhiFeng LTD.
 * @author:    郭志峰
 * @version    V1.0 
 * Createdate:         2016年6月15日 下午6:05:09
 * Modification  History:
 * Date         Author        Version        Discription
 * -----------------------------------------------------------------------------------
 * 2016年6月15日       guozhifeng         1.0             1.0
 * Why & What is modified: <修改原因描述>
 */
package com.jusfoun.jiulianhuan.shop.provider.service.goods;

import org.springframework.beans.factory.annotation.Autowired;

import com.jusfoun.jiulianhuan.shop.api.entity.GoodsAttr;
import com.jusfoun.jiulianhuan.shop.api.service.IGoodsAttrService;
import com.jusfoun.jiulianhuan.shop.provider.mapper.GoodsAttrMapper;
/**
 * 商品扩展属性
 * @ClassName: GoodsAttrServiceImpl 
 * @Description: 商品扩展属性
 * @author Timothy guozhifengvip@163.com 
 * @date 2016年6月15日 下午6:06:37 
 *
 */
public class GoodsAttrServiceImpl implements IGoodsAttrService {
	@Autowired
	private GoodsAttrMapper attrMapper;
	
	@Override
	public int deleteByPrimaryKey(Integer goodsAttrId) {
		return attrMapper.deleteByPrimaryKey(goodsAttrId);
	}

	@Override
	public int insert(GoodsAttr record) {
		return attrMapper.insert(record);
	}

	@Override
	public int insertSelective(GoodsAttr record) {
		return attrMapper.insertSelective(record);
	}

	@Override
	public GoodsAttr findByPrimaryKey(Integer goodsAttrId) {
		return attrMapper.selectByPrimaryKey(goodsAttrId);
	}

	@Override
	public int updateByPrimaryKeySelective(GoodsAttr record) {
		return attrMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKeyWithBLOBs(GoodsAttr record) {
		return attrMapper.updateByPrimaryKeyWithBLOBs(record);
	}

	@Override
	public int updateByPrimaryKey(GoodsAttr record) {
		return attrMapper.updateByPrimaryKey(record);
	}

}
