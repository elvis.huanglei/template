package com.jusfoun.jiulianhuan.shop.provider.config;

import com.alibaba.dubbo.config.*;
import com.alibaba.dubbo.config.spring.AnnotationBean;
import com.alibaba.dubbo.rpc.Exporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * @author huanglei
 * @created 16/6/11 上午3:33
 */
@Configuration
//@EnableAutoConfiguration
//@ConditionalOnClass(Exporter.class)
@ImportResource("classpath:dubbox/provider.xml")
public class DubboConfiguration  {

//  private static final Logger logger = LoggerFactory.getLogger(DubboConfiguration.class);
//
//  @Value("${dubbo.annotation.package}")
//  private String annotationPackage;
//
//  @Value("${dubbo.application.name}")
//  private String dubboApplicationName;
//
//  @Value("${dubbo.application.logger}")
//  private String dubboApplicationLogger;
//
//  @Value("${dubbo.protocol.name}")
//  private String dubboProtocolName;
//
//  @Value("${dubbo.protocol.port}")
//  private int dubboProtocolPort;
//
//  @Value("${dubbo.protocol.accessLog}")
//  private String dubboProtocolAccessLog;
//
//  @Value("${dubbo.provider.timeout}")
//  private int dubboProviderTimeout;
//
//  @Value("${dubbo.provider.retries}")
//  private int dubboProviderRetries;
//
//  @Value("${dubbo.provider.delay}")
//  private int dubboProviderDelay;
//
//  @Value("${dubbo.registr.protocol}")
//  private String dubboRegistrProtocol;
//
//  @Value("${dubbo.registry.address}")
//  private String dubboRegistryAddress;
//
//  @Value("${dubbo.registry.register}")
//  private boolean dubboRegistryRegister;
//
//  @Value("${dubbo.registry.subscribe}")
//  private boolean dubboRegistrySubscribe;
//
//
//
//  @Bean
//  public static AnnotationBean annotationBean(@Value("${dubbo.annotation.package}") String packageName) {
//    AnnotationBean annotationBean = new AnnotationBean();
//    annotationBean.setPackage(packageName);
//    logger.debug("[DubboAutoConfiguration] {}", packageName);
//
//    return annotationBean;
//  }
//
//  @Bean
//  public ApplicationConfig applicationConfig() {
//    ApplicationConfig applicationConfig = new ApplicationConfig();
//    applicationConfig.setName(dubboApplicationName);
//    applicationConfig.setLogger(dubboApplicationLogger);
//    logger.debug("[DubboAutoConfiguration] {}", dubboApplicationName);
//    return applicationConfig;
//  }
//
//  @Bean
//  public ProtocolConfig protocolConfig() {
//    ProtocolConfig protocolConfig = new ProtocolConfig();
//    protocolConfig.setName(dubboProtocolName);
//    protocolConfig.setPort(dubboProtocolPort);
//    protocolConfig.setThreads(200);
//    protocolConfig.setAccesslog(dubboProtocolAccessLog);
//    logger.debug("[DubboAutoConfiguration] {}", dubboProtocolName);
//    return protocolConfig;
//  }
//
//  @Bean
//  public ProviderConfig providerConfig(ApplicationConfig applicationConfig,
//                                       RegistryConfig registryConfig,
//                                       ProtocolConfig protocolConfig) {
//    ProviderConfig providerConfig = new ProviderConfig();
//    providerConfig.setTimeout(dubboProviderTimeout);
//    providerConfig.setRetries(dubboProviderRetries);
//    providerConfig.setDelay(dubboProviderDelay);
//    providerConfig.setApplication(applicationConfig);
//    providerConfig.setRegistry(registryConfig);
//    providerConfig.setProtocol(protocolConfig);
//    logger.debug("[DubboAutoConfiguration] {}", dubboProviderTimeout);
//    return providerConfig;
//  }
//
//  @Bean
//  public RegistryConfig registryConfig() {
//    RegistryConfig registryConfig = new RegistryConfig();
//    registryConfig.setProtocol(dubboRegistrProtocol);
//    registryConfig.setAddress(dubboRegistryAddress);
//    registryConfig.setRegister(dubboRegistryRegister);
//    registryConfig.setSubscribe(dubboRegistrySubscribe);
//    logger.debug("[DubboAutoConfiguration] {}", dubboRegistrProtocol);
//    return registryConfig;
//  }

//  @Bean
//  public MonitorConfig monitor(){
//    MonitorConfig monitor = new MonitorConfig();
//    monitor.setProtocol("registry");
//    return monitor;
//  }
}
