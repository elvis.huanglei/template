package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.Category;

public interface CategoryMapper {
    int deleteByPrimaryKey(Integer catId);

    int insert(Category record);

    int insertSelective(Category record);

    Category selectByPrimaryKey(Integer catId);

    int updateByPrimaryKeySelective(Category record);

    int updateByPrimaryKey(Category record);
}