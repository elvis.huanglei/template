package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.FavourableActivity;

public interface FavourableActivityMapper {
    int deleteByPrimaryKey(Integer actId);

    int insert(FavourableActivity record);

    int insertSelective(FavourableActivity record);

    FavourableActivity selectByPrimaryKey(Integer actId);

    int updateByPrimaryKeySelective(FavourableActivity record);

    int updateByPrimaryKeyWithBLOBs(FavourableActivity record);

    int updateByPrimaryKey(FavourableActivity record);
}