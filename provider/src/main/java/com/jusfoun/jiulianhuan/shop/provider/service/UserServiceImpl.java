package com.jusfoun.jiulianhuan.shop.provider.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jusfoun.jiulianhuan.shop.api.entity.Pagination;
import com.jusfoun.jiulianhuan.shop.api.entity.User;
import com.jusfoun.jiulianhuan.shop.api.service.UserService;
import com.jusfoun.jiulianhuan.shop.provider.mapper.UserMapper;

@Service(protocol = "dubbo")
public class UserServiceImpl implements UserService{
	@Autowired
	private UserMapper mapper;
	@Override
	public Pagination<User> findBypage(User user,Integer pageNum,Integer pageSize){
		PageHelper.startPage(pageNum, pageSize, "user_id");
        Page<User> page= (Page<User>) mapper.select(user);
        Pagination<User> p = new Pagination<User>();
        p.setTotal(page.getTotal());
        p.setList(page);
        return p;
	}

	@Override
	public int deleteByPrimaryKey(String userId) {
		if(StringUtils.isEmpty(userId)) return 0;
		String[] userids = userId.split(",");
		List<Integer> ids = new ArrayList<Integer>(userids.length);
		for(int i=0;i<userids.length;i++){
			ids.add(Integer.parseInt(userids[i]));
		}
		return mapper.deleteAll(ids);
	}

	@Override
	public int insert(User record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(User record) {
		return mapper.insertSelective(record);
	}

	@Override
	public User findByPrimaryKey(Integer userId) {
		return mapper.selectByPrimaryKey(userId);
	}

	@Override
	public int updateByPrimaryKeySelective(User record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(User record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public User findUserByName(String userName) {
		// TODO Auto-generated method stub
		return "zhangsan".equals(userName) || "xym".equals(userName) ? new User("xym".equals(userName) ? "xym" : "zhangsan") : null;
	}

}
