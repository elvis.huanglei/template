package com.jusfoun.jiulianhuan.shop.provider.service.article;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jusfoun.jiulianhuan.shop.api.entity.ArticleCat;
import com.jusfoun.jiulianhuan.shop.api.entity.Pagination;
import com.jusfoun.jiulianhuan.shop.api.service.IArticleCatService;
import com.jusfoun.jiulianhuan.shop.provider.mapper.ArticleCatMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tao on 16/6/15.
 * 文章列表serviceimpl
 */
@Service(protocol = "dubbo")
public class ArticleCatServiceImpl implements IArticleCatService {

    @Autowired
    private ArticleCatMapper mapper;

    @Override
    public int deleteByPrimaryKey(Integer articleCatId) {
        return mapper.deleteByPrimaryKey(articleCatId);
    }

    @Override
    public int insert(ArticleCat articleCat) {
        return mapper.insert(articleCat);
    }

    @Override
    public int insertSelective(ArticleCat articleCat) {
        return mapper.insertSelective(articleCat);
    }

    @Override
    public ArticleCat findByPrimaryKey(Integer articleCatId) {
        return mapper.selectByPrimaryKey(articleCatId);
    }

    @Override
    public int updateByPrimaryKeySelective(ArticleCat articleCat) {
        return mapper.updateByPrimaryKeySelective(articleCat);
    }

    @Override
    public int updateByPrimaryKey(ArticleCat articleCat) {
        return mapper.updateByPrimaryKey(articleCat);
    }

    @Override
    public Pagination<ArticleCat> findBypage(ArticleCat articleCat, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize, "cat_id");
        Page<ArticleCat> page= (Page<ArticleCat>) mapper.select(articleCat);
        Pagination<ArticleCat> p = new Pagination<ArticleCat>();
        p.setTotal(page.getTotal());
        p.setList(page);
        return p;
    }

    @Override
    public int deleteAllArticleCat(List<Integer> catids) {
        return mapper.deleteAll(catids);
    }
}
