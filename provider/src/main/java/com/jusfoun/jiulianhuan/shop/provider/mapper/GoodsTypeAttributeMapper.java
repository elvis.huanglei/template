package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.GoodsTypeAttribute;

public interface GoodsTypeAttributeMapper {
    int deleteByPrimaryKey(Integer attrId);

    int insert(GoodsTypeAttribute record);

    int insertSelective(GoodsTypeAttribute record);

    GoodsTypeAttribute selectByPrimaryKey(Integer attrId);

    int updateByPrimaryKeySelective(GoodsTypeAttribute record);

    int updateByPrimaryKeyWithBLOBs(GoodsTypeAttribute record);

    int updateByPrimaryKey(GoodsTypeAttribute record);
}