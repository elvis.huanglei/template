package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.GroupGoods;
import com.jusfoun.jiulianhuan.shop.api.entity.GroupGoodsKey;

public interface GroupGoodsMapper {
    int deleteByPrimaryKey(GroupGoodsKey key);

    int insert(GroupGoods record);

    int insertSelective(GroupGoods record);

    GroupGoods selectByPrimaryKey(GroupGoodsKey key);

    int updateByPrimaryKeySelective(GroupGoods record);

    int updateByPrimaryKey(GroupGoods record);
}