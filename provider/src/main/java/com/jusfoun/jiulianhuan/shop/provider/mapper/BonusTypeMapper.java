package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.BonusType;

public interface BonusTypeMapper {
    int deleteByPrimaryKey(Integer typeId);

    int insert(BonusType record);

    int insertSelective(BonusType record);

    BonusType selectByPrimaryKey(Integer typeId);

    int updateByPrimaryKeySelective(BonusType record);

    int updateByPrimaryKey(BonusType record);
}