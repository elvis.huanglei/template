package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.EmailSendlist;

public interface EmailSendlistMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(EmailSendlist record);

    int insertSelective(EmailSendlist record);

    EmailSendlist selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(EmailSendlist record);

    int updateByPrimaryKeyWithBLOBs(EmailSendlist record);

    int updateByPrimaryKey(EmailSendlist record);
}