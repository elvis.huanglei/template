package com.jusfoun.jiulianhuan.shop.provider.mapper;

import java.util.List;

import org.apache.ibatis.annotations.DeleteProvider;

import com.jusfoun.jiulianhuan.shop.api.entity.User;
import com.jusfoun.jiulianhuan.shop.provider.ext.MyMapper;
import com.jusfoun.jiulianhuan.shop.provider.mapper.provider.UserProvider;

public interface UserMapper extends MyMapper<User>{
	@DeleteProvider(type = UserProvider.class, method = "deleteAll")  //"delete from user where user_id in (${userId})"
	int deleteAll(List<Integer> userIds);
}