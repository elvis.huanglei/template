package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.Cart;
import com.jusfoun.jiulianhuan.shop.provider.ext.MyMapper;

public interface CartMapper extends MyMapper<Cart>{
    int deleteByPrimaryKey(Integer recId);

    int insert(Cart record);

    int insertSelective(Cart record);

    Cart selectByPrimaryKey(Integer recId);

    int updateByPrimaryKeySelective(Cart record);

    int updateByPrimaryKeyWithBLOBs(Cart record);

    int updateByPrimaryKey(Cart record);
}