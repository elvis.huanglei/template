package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.GoodsGallery;

public interface GoodsGalleryMapper {
    int deleteByPrimaryKey(Integer imgId);

    int insert(GoodsGallery record);

    int insertSelective(GoodsGallery record);

    GoodsGallery selectByPrimaryKey(Integer imgId);

    int updateByPrimaryKeySelective(GoodsGallery record);

    int updateByPrimaryKey(GoodsGallery record);
}