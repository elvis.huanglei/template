package com.jusfoun.jiulianhuan.shop.provider.mapper.provider;

import java.util.List;
import java.util.Map;

public class UserProvider {
	public String deleteAll(Map map){
		List<Integer> list = (List<Integer>) map.get("list");
		StringBuilder sb = new StringBuilder("delete from user where user_id in (");
		for(int i=0;i<list.size();i++){
			sb.append(list.get(i));
			if(i!=list.size()-1){
				sb.append(",");
			}
		}
		sb.append(")");
		return sb.toString();
	}
}
