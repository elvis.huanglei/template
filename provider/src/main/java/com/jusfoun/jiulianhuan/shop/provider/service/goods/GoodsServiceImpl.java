/**
 * All rights Reserved, Designed By ZTE-ITS
 * Copyright:    Copyright(C) 2016-2020
 * Company       JUSFOUN GuoZhiFeng LTD.
 * @author:    郭志峰
 * @version    V1.0 
 * Createdate:         2016年6月15日 下午4:19:04
 * Modification  History:
 * Date         Author        Version        Discription
 * -----------------------------------------------------------------------------------
 * 2016年6月15日       guozhifeng         1.0             1.0
 * Why & What is modified: <修改原因描述>
 */
package com.jusfoun.jiulianhuan.shop.provider.service.goods;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.dubbo.rpc.protocol.rest.support.ContentType;
import com.jusfoun.jiulianhuan.shop.api.entity.Goods;
import com.jusfoun.jiulianhuan.shop.api.service.IGoodsService;
import com.jusfoun.jiulianhuan.shop.provider.mapper.GoodsMapper;

/**
 * 
 * @ClassName: GoodsServiceImpl 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author Timothy guozhifengvip@163.com 
 * @date 2016年6月15日 下午4:19:11 
 *
 */
public class GoodsServiceImpl  implements IGoodsService{
	@Autowired
	private GoodsMapper goodsMapper;
	
	/**
	 * 根据主键删除商品
	 * @Title: deleteByPrimaryKey 
	 * @Description: TODO(这里用一句话描述这个方法的作用) 
	 * @param @param goodsId
	 * @param @return    设定文件 
	 * @return int    返回类型 
	 * @throws
	 */
	public int deleteByPrimaryKey(Integer goodsId){
		return goodsMapper.deleteByPrimaryKey(goodsId);
	}
   /**
    * 添加商品
    * @Title: insert 
    * @Description: TODO(这里用一句话描述这个方法的作用) 
    * @param @param record
    * @param @return    设定文件 
    * @return int    返回类型 
    * @throws
    */
	public int insert(Goods record){
		return goodsMapper.insert(record);
		
	}
    /**
     * 插入一条数据,只插入不为null的字段,不会影响有默认值的字段
     * @Title: insertSelective 
     * @Description: TODO(这里用一句话描述这个方法的作用) 
     * @param @param record
     * @param @return    设定文件 
     * @return int    返回类型 
     * @throws
     */
	public int insertSelective(Goods record){
		return goodsMapper.insertSelective(record);
		
	}
    /**
     * 根据主键查询商品
     * @Title: findByPrimaryKey 
     * @Description: TODO(这里用一句话描述这个方法的作用) 
     * @param @param goodsId
     * @param @return    设定文件 
     * @return Goods    返回类型 
     * @throws
     */
	public Goods findByPrimaryKey(Integer goodsId){
		return goodsMapper.selectByPrimaryKey(goodsId);
		
	}
    /**
     * 根据主键进行更新只会更新不是null的数据
     * @Title: updateByPrimaryKeySelective 
     * @Description: TODO(这里用一句话描述这个方法的作用) 
     * @param @param record
     * @param @return    设定文件 
     * @return int    返回类型 
     * @throws
     */
	public int updateByPrimaryKeySelective(Goods record){
		return goodsMapper.updateByPrimaryKey(record);
		
	}
    /**
     *    
     * @Title: updateByPrimaryKeyWithBLOBs 
     * @Description: TODO(这里用一句话描述这个方法的作用) 
     * @param @param record
     * @param @return    设定文件 
     * @return int    返回类型 
     * @throws
     */
	public int updateByPrimaryKeyWithBLOBs(Goods record){
		return goodsMapper.updateByPrimaryKeyWithBLOBs(record);
		
	}
    /**
     * 根据主键进行更新
     * @Title: updateByPrimaryKey 
     * @Description: TODO(这里用一句话描述这个方法的作用) 
     * @param @param record
     * @param @return    设定文件 
     * @return int    返回类型 
     * @throws
     */
	public int updateByPrimaryKey(Goods record){
		return goodsMapper.updateByPrimaryKey(record);
		
	}

}
