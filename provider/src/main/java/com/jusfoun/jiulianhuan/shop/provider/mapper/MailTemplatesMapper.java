package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.MailTemplates;

public interface MailTemplatesMapper {
    int deleteByPrimaryKey(Integer templateId);

    int insert(MailTemplates record);

    int insertSelective(MailTemplates record);

    MailTemplates selectByPrimaryKey(Integer templateId);

    int updateByPrimaryKeySelective(MailTemplates record);

    int updateByPrimaryKeyWithBLOBs(MailTemplates record);

    int updateByPrimaryKey(MailTemplates record);
}