package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.UserBonus;

public interface UserBonusMapper {
    int deleteByPrimaryKey(Integer bonusId);

    int insert(UserBonus record);

    int insertSelective(UserBonus record);

    UserBonus selectByPrimaryKey(Integer bonusId);

    int updateByPrimaryKeySelective(UserBonus record);

    int updateByPrimaryKey(UserBonus record);
}