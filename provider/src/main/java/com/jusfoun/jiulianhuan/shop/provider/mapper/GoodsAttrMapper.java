package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.GoodsAttr;

public interface GoodsAttrMapper {
    int deleteByPrimaryKey(Integer goodsAttrId);

    int insert(GoodsAttr record);

    int insertSelective(GoodsAttr record);

    GoodsAttr selectByPrimaryKey(Integer goodsAttrId);

    int updateByPrimaryKeySelective(GoodsAttr record);

    int updateByPrimaryKeyWithBLOBs(GoodsAttr record);

    int updateByPrimaryKey(GoodsAttr record);
}