package com.jusfoun.jiulianhuan.shop.provider.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.jusfoun.jiulianhuan.shop.api.entity.Cart;
import com.jusfoun.jiulianhuan.shop.api.service.CartService;
import com.jusfoun.jiulianhuan.shop.provider.mapper.CartMapper;

@Service(protocol = "dubbo")
public class CartServiceImpl implements CartService{
	@Autowired
	private CartMapper mapper;

	@Override
	public int deleteByPrimaryKey(Integer CartId) {
		// TODO Auto-generated method stub
		return mapper.deleteByPrimaryKey(CartId);
	}

	@Override
	public int insert(Cart cart) {
		// TODO Auto-generated method stub
		return mapper.insert(cart);
	}

	@Override
	public int insertSelective(Cart cart) {
		// TODO Auto-generated method stub
		return mapper.insertSelective(cart);
	}

	@Override
	public Cart selectByPrimaryKey(Integer CartId) {
		// TODO Auto-generated method stub
		return mapper.selectByPrimaryKey(CartId);
	}

	@Override
	public int updateByPrimaryKeySelective(Cart record) {
		// TODO Auto-generated method stub
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Cart record) {
		// TODO Auto-generated method stub
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public int updateByPrimaryKeyWithBLOBs(Cart record) {
		// TODO Auto-generated method stub
		return mapper.updateByPrimaryKeySelective(record);
	}

}
