package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.GoodsArticleKey;

public interface GoodsArticleMapper {
    int deleteByPrimaryKey(GoodsArticleKey key);

    int insert(GoodsArticleKey record);

    int insertSelective(GoodsArticleKey record);
}