package com.jusfoun.jiulianhuan.shop.provider.filter;

import com.alibaba.druid.support.http.WebStatFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * @author huanglei
 * @created 16/6/13 下午3:38
 */
@WebFilter(filterName="druidWebStatFilter",urlPatterns="/*",
  initParams={
    @WebInitParam(name="exclusions",value="*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*")// 忽略资源
  })
public class DruidStatFilter extends WebStatFilter {

}
