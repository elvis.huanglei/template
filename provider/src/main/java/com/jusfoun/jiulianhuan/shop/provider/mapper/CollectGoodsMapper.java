package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.CollectGoods;

public interface CollectGoodsMapper {
    int deleteByPrimaryKey(Integer recId);

    int insert(CollectGoods record);

    int insertSelective(CollectGoods record);

    CollectGoods selectByPrimaryKey(Integer recId);

    int updateByPrimaryKeySelective(CollectGoods record);

    int updateByPrimaryKey(CollectGoods record);
}