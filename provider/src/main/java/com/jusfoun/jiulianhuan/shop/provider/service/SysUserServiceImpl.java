package com.jusfoun.jiulianhuan.shop.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.jusfoun.jiulianhuan.shop.api.entity.SysUser;
import com.jusfoun.jiulianhuan.shop.provider.mapper.SysUserMapper;
import com.jusfoun.jiulianhuan.shop.api.service.SysUserService;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author huanglei
 * @created 16/6/10 上午9:39
 */

@Service(protocol = "dubbo")
public class SysUserServiceImpl implements SysUserService {

//  @Autowired
//  private SqlSessionTemplate sqlSessionTemplate;
//
//  private SysUserMapper getMapper() {
//    return sqlSessionTemplate.getMapper(SysUserMapper.class);
//  }
  @Autowired
  private SysUserMapper sysUserMapper;


  public SysUser findSysUserById(int id) {
    return sysUserMapper.findSysUserById(id);
  }


  public SysUser findSysUserByEmail(String email) {
    return sysUserMapper.findSysUserByEmail(email);
  }

  //TODO... 更新失败验证
  public void update(SysUser user) {
    sysUserMapper.update(user);
  }


}
