package com.jusfoun.jiulianhuan.shop.provider.mapper;

import com.jusfoun.jiulianhuan.shop.api.entity.ArticleCat;
import com.jusfoun.jiulianhuan.shop.provider.ext.MyMapper;
import com.jusfoun.jiulianhuan.shop.provider.mapper.provider.ArticleCatProvider;
import com.jusfoun.jiulianhuan.shop.provider.mapper.provider.UserProvider;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;

import java.util.List;

/**
 * taocl
 * 文章类型Mapper
 */
public interface ArticleCatMapper extends MyMapper<ArticleCat> {

    @DeleteProvider(type = ArticleCatProvider.class, method = "deleteAll")
    int deleteAll(List<Integer> catids);
}