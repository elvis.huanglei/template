package com.jusfoun.jiulianhuan.shop.provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Locale;

/**
 * @author huanglei
 * @created 16/6/11 上午2:50
 */
@SpringBootApplication
//@Configuration
//@EnableAutoConfiguration
//@ComponentScan
public class ProviderApplication extends SpringBootServletInitializer{

  private static final Logger logger = LoggerFactory.getLogger(ProviderApplication.class);

  public static void main(String[] args) {

    ApplicationContext context = SpringApplication.run(ProviderApplication.class, args);

//    SysUserService sysUserService = (SysUserService)ctx.getBean("SysUserServiceImpl");
//    SysUser su = sysUserService.findSysUserById(1);
//    BCryptPasswordEncoder bc = new BCryptPasswordEncoder(4);
//    su.setPassword(bc.encode("111111"));
//    sysUserService.update(su);

    if (context instanceof EmbeddedWebApplicationContext) {
      int port = ((EmbeddedWebApplicationContext) context).getEmbeddedServletContainer().getPort();
      String contextPath = context.getApplicationName();
      String url = String.format(Locale.US, "http://localhost:%d%s", port, contextPath);

      //提示项目用到的相关配置文件
      logger.info(" =========== ${user.dir}={} ===========  ", System.getProperty("user.dir"));
      logger.info(" =========== ${java.io.tmpdir}={} ===========  ", System.getProperty("java.io.tmpdir"));

      String dashes = "------------------------------------------------------------------------";
      logger.info("Access URLs:\n{}\n\tLocal: \t\t{}\n{}", dashes, url, dashes);
    }
  }

}
