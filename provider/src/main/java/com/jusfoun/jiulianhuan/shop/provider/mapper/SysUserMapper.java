package com.jusfoun.jiulianhuan.shop.provider.mapper;


import com.jusfoun.jiulianhuan.shop.api.entity.SysUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * @author huanglei
 * @created 16/6/10 上午9:30
 */
public interface SysUserMapper {

  @Select("SELECT * FROM sys_user WHERE id = #{id}")
  SysUser findSysUserById(int id);

  @Select("SELECT * FROM sys_user WHERE email=#{email}")
  SysUser findSysUserByEmail(@Param("email") String email);

  @Update("UPDATE sys_user set name=#{name},password=#{password} WHERE id=#{id}")
  void update(SysUser user);
}
