package com.jusfoun.jiulianhuan.shop.provider.config;

import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.druid.pool.DruidDataSource;
import com.github.pagehelper.PageHelper;

/**
 * MybatisConfiguration
 *
 * @author Connor
 * @version 1.0
 * @date 16/1/28 下午3:09
 **/
@Configuration("MybatisConfiguration")
@MapperScan(basePackages={"com.jusfoun.jiulianhuan.shop.provider.mapper"})
public class MybatisConfiguration{

  private static final Logger logger = LoggerFactory.getLogger(MybatisConfiguration.class);

  @Value("${druid.jdbc.driverClassName}")
  private String driverClassName;

  @Value("${druid.jdbc.url}")
  private String jdbcUrl;

  @Value("${druid.jdbc.userName}")
  private String jdbcUsername;

  @Value("${druid.jdbc.passWord}")
  private String jdbcPassword;

  @Value("${druid.jdbc.filters}")
  private String filters;

  @Value("${druid.jdbc.maxActive}")
  private int maxActive;

  @Value("${druid.jdbc.initialSize}")
  private int initialSize;

  @Value("${druid.jdbc.maxWait}")
  private int maxWait;

  @Value("${druid.jdbc.minIdle}")
  private int minIdle;

  @Value("${druid.jdbc.timeBetweenEvictionRunsMillis}")
  private long timeBetweenEvictionRunsMillis;

  @Value("${druid.jdbc.minEvictableIdleTimeMillis}")
  private int minEvictableIdleTimeMillis;

  @Value("${druid.jdbc.validationQuery}")
  private String validationQuery;

  @Value("${druid.jdbc.testWhileIdle}")
  private boolean testWhileIdle;

  @Value("${druid.jdbc.testOnBorrow}")
  private boolean testOnBorrow;

  @Value("${druid.jdbc.testOnReturn}")
  private boolean testOnReturn;

  @Value("${druid.jdbc.maxOpenPreparedStatements}")
  private int maxOpenPreparedStatements;

  @Value("${druid.jdbc.removeAbandoned}")
  private boolean removeAbandoned;

  @Value("${druid.jdbc.removeAbandonedTimeout}")
  private int removeAbandonedTimeout;

  @Value("${druid.jdbc.logAbandoned}")
  private boolean logAbandoned;

  @Bean(name = "dataSource",destroyMethod="close")
  public DataSource dataSource() throws SQLException{
    logger.debug("---------> Set BoneCP Data Pool");
    DruidDataSource source = new DruidDataSource();

    source.setUsername(jdbcUsername);
    source.setPassword(jdbcPassword);
    source.setUrl(jdbcUrl);
    source.setDriverClassName(driverClassName);
    source.setFilters(filters);
    source.setMaxActive(maxActive);
    source.setInitialSize(initialSize);
    source.setMinIdle(minIdle);
    source.setMaxWait(maxWait);
    source.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
    source.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
    source.setValidationQuery(validationQuery);
    source.setTestWhileIdle(testWhileIdle);
    source.setTestOnReturn(testOnReturn);
    source.setTestOnBorrow(testOnBorrow);
    source.setMaxOpenPreparedStatements(maxOpenPreparedStatements);
    source.setRemoveAbandoned(removeAbandoned);
    source.setRemoveAbandonedTimeout(removeAbandonedTimeout);
    source.setLogAbandoned(logAbandoned);

    return source;
  }

  @Bean
  public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
	  
    logger.debug("---------> Set SqlSessionFactory");
    SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
    sessionFactory.setDataSource(dataSource);
    
  //分页插件
    PageHelper pageHelper = new PageHelper();
    Properties properties = new Properties();
    properties.setProperty("reasonable", "true");
    properties.setProperty("supportMethodsArguments", "true");
    properties.setProperty("returnPageInfo", "check");
    properties.setProperty("params", "count=countSql");
    pageHelper.setProperties(properties);

    //添加插件
    sessionFactory.setPlugins(new Interceptor[]{pageHelper});
    return sessionFactory.getObject();
  }

  @Bean(destroyMethod="clearCache")
  public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
    logger.debug("---------> Set SqlSessionTemplate");
    sqlSessionFactory.getConfiguration().setMapUnderscoreToCamelCase(true);
    return new SqlSessionTemplate(sqlSessionFactory);
  }

}