package com.jusfoun.jiulianhuan.shop.provider.test;

import com.jusfoun.jiulianhuan.shop.api.entity.ArticleCat;
import com.jusfoun.jiulianhuan.shop.api.entity.Pagination;
import com.jusfoun.jiulianhuan.shop.api.service.IArticleCatService;
import com.jusfoun.jiulianhuan.shop.provider.ProviderApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Arrays;
import java.util.List;

/**
 * Created by tao on 16/6/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ProviderApplication.class)
@WebAppConfiguration
public class ArticleCatTest {

    @Autowired
    IArticleCatService catService;

    @Test
    public void insertArticleCat(){
        ArticleCat ac = createArticleCat();
        System.out.println(catService.insert(ac));//成功返回1
    }

    @Test
    public void insertSelectArticleCat(){
        ArticleCat ac = createArticleCat();
        System.out.println(catService.insertSelective(ac));//成功返回1
    }

    @Test
    public void deleteArticleCat(){
        catService.deleteByPrimaryKey(7);
    }

    @Test
    public void updateArticleCat(){
        ArticleCat ac = createArticleCat();
        ac.setCatId(12);
        ac.setCatDesc("重置");
        catService.updateByPrimaryKey(ac);
    }
    @Test
    public void updateSelectArticleCat(){
        ArticleCat ac = createArticleCat();
        ac.setCatId(3);
        catService.updateByPrimaryKeySelective(ac);
    }


    @Test
    public void selArticleCatById(){
        System.out.println(catService.findByPrimaryKey(12));
    }

    @Test
    public void selArticleCatPage() {
        ArticleCat ac = new ArticleCat();
        Pagination<ArticleCat> bypage = catService.findBypage(ac,1000, 3);
        List<ArticleCat> list = bypage.getList();
        System.out.println("~~~~~~~"+list.size());
        for(ArticleCat cat :list) {
            System.out.println(cat);
        }
    }

    @Test
    public void deleteAllArticleCat(){
        catService.deleteAllArticleCat(Arrays.asList(5,6));
    }


    public ArticleCat createArticleCat(){
        ArticleCat ac = new ArticleCat();
        ac.setCatName("apitaocl8899");
        ac.setCatDesc("描述558899");
        //ac.setCatFlag(1);
        //ac.setParentId(1);

        return ac;
    }
}
