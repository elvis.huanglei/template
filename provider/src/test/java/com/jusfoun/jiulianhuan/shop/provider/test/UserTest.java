package com.jusfoun.jiulianhuan.shop.provider.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.jusfoun.jiulianhuan.shop.api.entity.Pagination;
import com.jusfoun.jiulianhuan.shop.api.entity.User;
import com.jusfoun.jiulianhuan.shop.api.service.UserService;
import com.jusfoun.jiulianhuan.shop.provider.ProviderApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ProviderApplication.class)
@WebAppConfiguration
public class UserTest {
	@Autowired
	UserService service ;
	@Test
	public void findByPage(){
		Pagination<User> users = service.findBypage(null, 1, 15);
		System.out.println(users.getTotal());
	}
	
	@Test
	public void delete(){
		service.deleteByPrimaryKey("11,22,33");
	}
}
